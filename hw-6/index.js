
const ipBtn = document.querySelector('#search-ip-btn');
const getIpUrl = 'https://api.ipify.org/?format=json';
const container = document.querySelector('.container');

ipBtn.addEventListener('click', async () => {
	try {
		const { data: { ip } } = await axios.get(getIpUrl);

		const { data } = await axios.get(`http://ip-api.com/json/${ip}?fields=status,message,continent,country,region,regionName,city,district`);
		
		container.innerHTML = '';
		container.insertAdjacentHTML('beforeend', `
			<p>Continent: ${data.continent}</p>
			<p>Country: ${data.country}</p>
			<p>Region: ${data.regionName}</p>
			<p>City: ${data.city}</p>
			<p>District: ${data.district ? data.district : 'no data available'}</p>
		`)
	} catch(err) {
		console.log(err);
	}
})
