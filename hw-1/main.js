'use strict';

class Employee {
	constructor(name, age, salary) {
		this._name = name;
		this._age = age;
		this._salary = salary;
	}

	get name() {
		return this._name;
	}
	set name(value) {
		return this._name = value;
	}

	get age() {
		return this._age;
	}
	set age(value) {
		return this._age = value;
	}

	get salary() {
		return this._salary;
	}
	set salary(value) {
		return this._salary = value;
	}
}


class Programmer extends Employee {
	constructor(name, age, salary, lang) {
		super(name, age, salary);
		this.lang = lang;
	}

	get salary() {
		return `${super.salary * 3}$`;
	}
}


const programmer1 = new Programmer('John', 25, 1000, 'English');
console.log(programmer1);

const programmer2 = new Programmer('Bob', 32, 2000, 'English, Italian');
console.log(programmer1);

const programmer3 = new Programmer('Mike', 28, 1500, 'English, Ukrainian');
console.log(programmer1);

console.log(programmer1.salary);
console.log(programmer2.salary);
console.log(programmer3.salary);

