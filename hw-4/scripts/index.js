'use strict';

fetch('https://ajax.test-danit.com/api/swapi/films')
	.then(resp => {
		if(resp.ok) {
			return resp.json();
		} else {
			throw new Error('Error! No such data!');
		}
	})
	.then(data => {
		data.sort((a, b) => {
			return a.episodeId - b.episodeId;
		})
		.forEach(({ episodeId, name, openingCrawl, characters }) => {
			new FilmCard(episodeId, name, openingCrawl, characters).render('.container');
		})
	})
	.catch(err => console.log(err));

class FilmCard {
	constructor(id, name, summary, characters) {
		this.id = id;
		this.name = name;
		this.summary = summary;
		this.characters = characters;
		this.cardContainer = document.createElement("div");
		this.filmsListWrap = document.createElement("div");
		this.loaderWrap = document.createElement("div");
	}

	createElements() {
		this.cardContainer.className = "card";
		this.cardContainer.innerHTML = `<p><span>Episode:</span>${this.id}</p>
										<p><span>Title:</span>${this.name}</p>
										<p>${this.summary}</p>`;
		this.filmsListWrap.innerHTML = `<span class="films-title">Main characters: </span>`;
		this.loaderWrap.className = 'loader-wrap';
		this.loaderWrap.innerHTML = `<span class="loader"></span>`
		this.cardContainer.append(this.filmsListWrap, this.loaderWrap);
	}

	getCharacters() {
		const charactersRequestArr = this.characters.map(url => fetch(url).then(res => res.json()));
		
		Promise.allSettled(charactersRequestArr)
			.then(resp => {
				const namesArr = resp.filter(el => el.status === 'fulfilled')
					.map(({value}) => value?.name);

				this.loaderWrap.style.display = 'none';
				this.filmsListWrap.insertAdjacentHTML('beforeend', `<div>${namesArr.join(', ')}</div>`)
			})
			.catch(err => console.log(err));
	}
	
	render(selector) {
		this.createElements();
		document.querySelector(selector).append(this.cardContainer);
		this.getCharacters();
	}
}
