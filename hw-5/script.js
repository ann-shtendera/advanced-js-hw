'use strict';


const usersUrl = 'https://ajax.test-danit.com/api/json/users';
const postsUrl = 'https://ajax.test-danit.com/api/json/posts';


// CLASS CARD --------------------------------------------------------------------

class Card {
	constructor(postId, userId, name, nick, email, title, post, EditPostModal) {
		this.postId = postId;
		this.userId = userId;
		this.name = name;
		this.nick = nick;
		this.email = email;
		this.title = title;
		this.post = post;
		this.EditPostModal = EditPostModal;
		this.container = document.createElement('div');
		this.cardTitle = document.createElement('p');
		this.cardBody = document.createElement('p');
		this.editBtn = document.createElement('button');
		this.deleteBtn = document.createElement('button');
	}

	createElements() {
		this.container.className = 'card';
		this.container.innerHTML = `
		<div class="card__header">
			<span class="card__user-name">${this.name}</span>
			<span class="card__user-nickname">${this.nick}</span>
			<span class="card__user-email">${this.email}</span>
		</div>
		`;
		const cardBody = document.createElement('div');
		cardBody.className = 'card__body';
		this.cardTitle.className = "card__body-title";
		this.cardTitle.innerText = this.title;
		this.cardBody.innerText = this.post;
		cardBody.append(this.cardTitle, this.cardBody);

		const btnsContainer = document.createElement('div');
		btnsContainer.className = 'card__buttons-container';
		this.editBtn.innerText = 'Edit post';
		this.deleteBtn.innerText = 'Delete post';
		btnsContainer.append(this.editBtn, this.deleteBtn);

		this.container.append(cardBody, btnsContainer);
	}

	addListeners() {
		this.deleteBtn.addEventListener('click', () => {
			axios.delete(`${postsUrl}/${this.postId}`)
				.then(({ status }) => {
					if(status === 200) {
						this.removeCard();
					}
				})
				.catch(({ name, message, response}) => {
					console.log(`${name}: ${message}. ${response.data}`);
				})
		})

		this.editBtn.addEventListener('click', () => {
			new EditPostModal(this).render();
		})
	}

	removeCard() {
		this.container.remove();
	}

	editCard(newTitle, newPost) {
		this.title = newTitle;
		this.post = newPost;
		this.cardTitle.innerText = this.title;
		this.cardBody.innerText = this.post;
	}

	render(selector) {
		this.createElements();
		this.addListeners();
		document.querySelector(selector).prepend(this.container);
	}
}

// CLASS MODAL --------------------------------------------------------------------

class Modal {
	constructor() {
		this.modalContainer = document.createElement('div');
		this.bgContainer = document.createElement('div');
		this.modalContentContainer = document.createElement('div');
		this.closeBtn = document.createElement('button');
		this.contentWrapper = document.createElement('div');
	}

	createElements() {
		this.modalContainer.className = "modal";
		this.bgContainer.className = "modal__background";
		this.modalContentContainer.className = "modal__main-container";
		this.closeBtn.className = "modal__close";
		this.closeBtn.innerText = 'x';
		this.contentWrapper.className = "modal__content-wrapper";

		this.modalContentContainer.append(this.closeBtn, this.contentWrapper);
		this.modalContainer.append(this.bgContainer, this.modalContentContainer);
	}

	deleteModal() {
		this.modalContainer.remove();
	}

	addListeners() {
		this.bgContainer.addEventListener('click', () => {
			this.deleteModal();
		})
		this.closeBtn.addEventListener('click', () => {
			this.deleteModal();
		})
	}
	render(selector = 'body') {
		this.createElements();
		this.addListeners();
		document.querySelector(selector).append(this.modalContainer);
	}
}

class PostModal extends Modal {
	constructor() {
		super();
		this.form = document.createElement('form');
		this.input = document.createElement('input');
		this.textarea = document.createElement('textarea');
		this.confirmBtn = document.createElement('button');
	}

	createForm() {
		const inputLabel = document.createElement('label');
		inputLabel.innerText = 'Title';
		const textareaLabel = document.createElement('label');
		textareaLabel.innerText = 'Post';
		this.input.name = 'title';
		this.textarea.name = 'body';
		this.textarea.rows = '4';
		this.form.append(inputLabel, this.input, textareaLabel, this.textarea, this.confirmBtn);
	}

	createElements() {
		super.createElements();
		this.createForm();
		this.confirmBtn.innerText = 'Confirm';
		this.contentWrapper.append(this.form);
	}
}

// Модальне вікно для додавання нового посту ----------------------------------------------

class AddPostModal extends PostModal {
	constructor(userId) {
		super();
		this.userId = userId;
	}
	
	addListeners() {
		super.addListeners();

		this.form.addEventListener('submit', e => {
			e.preventDefault();

			const body = {
				[this.input.name]: this.input.value,
				[this.textarea.name]: this.textarea.value,
				userId: this.userId,
			};

			axios.post(postsUrl, body)
				.then(({ data: post }) => {

					axios.get(`${usersUrl}/${this.userId}`)
						.then(({ data: user }) => {
							new Card(post.id, this.userId, user.name, user.username, user.email, post.title, post.body, EditPostModal)
								.render('.posts')
							this.deleteModal();
						})
						.catch(({ name, message, response}) => {
							console.log(`${name}: ${message}. ${response.data}`);
						})
				})
				.catch(({ name, message, response}) => {
					console.log(`${name}: ${message}. ${response.data}`);
				})
		})
	}
 }

 // Модальне вікно для редагування посту ----------------------------------------------

 class EditPostModal extends PostModal {
	constructor(card) {
		super();
		this.card = card;
		this.postId = this.card.postId;
		this.userId = this.card.userId;
		this.title = this.card.title;
		this.post = this.card.post;
	}

	createElements() {
		super.createElements();
		this.input.value = this.title;
		this.textarea.value = this.post;
	}
	
	addListeners() {
		super.addListeners();

		this.form.addEventListener('submit', e => {
			e.preventDefault();

			const body = {
				[this.input.name]: this.input.value,
				[this.textarea.name]: this.textarea.value,
				userId: this.userId,
			};

			axios.put(`${postsUrl}/${this.postId}`, body)
				.then(({ data }) => {
					this.card.editCard(data.title, data.body);
					this.deleteModal();
				})
				.catch(({ name, message, response}) => {
					console.log(`${name}: ${message}. ${response.data}`);
				})
		})
	}
 }


// Відмалювання всіх карток на сторінці при завантаженні сторінки ---------------------------

axios.get(usersUrl)
	.then(({ data: users }) => {

		axios.get(postsUrl)
			.then(({ data: posts }) => {
				document.querySelector('.preloader').style.display = 'none';

				users.forEach(({ id: userId, name, username, email }) => {
					const userPosts = posts.filter(({ userId: postUserId }) => postUserId === userId)

					userPosts.forEach(({ id: postId, title, body }) => {
						new Card(postId, userId, name, username, email, title, body, EditPostModal)
							.render('.posts');
					})
				})
			})
			.catch(({ name, message, response}) => {
				console.log(`${name}: ${message}. ${response.data}`);
			})
	})
	.catch(({ name, message, response}) => {
		console.log(`${name}: ${message}. ${response.data}`);
	})


// Кнопка додавання нових постів --------------------------------------------------------

const addPostBtn = document.querySelector('.add-post-btn')

addPostBtn.addEventListener('click', () => {
	new AddPostModal(1).render();
})