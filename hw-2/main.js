'use strict';

const books = [
	{ 
	  author: "Люсі Фолі",
	  name: "Список запрошених",
	  price: 70 
	}, 
	{
	 author: "Сюзанна Кларк",
	 name: "Джонатан Стрейндж і м-р Норрелл",
	}, 
	{ 
	  name: "Дизайн. Книга для недизайнерів.",
	  price: 70
	}, 
	{ 
	  author: "Алан Мур",
	  name: "Неономікон",
	  price: 70
	}, 
	{
	 author: "Террі Пратчетт",
	 name: "Рухомі картинки",
	 price: 40
	},
	{
	 author: "Анґус Гайленд",
	 name: "Коти в мистецтві",
	}
  ];


class MissingDataError extends Error {
	constructor(message) {
		super();
		this.name = 'MissingDataError';
		this.message = message;
	}
}

class Book {
	constructor(author, name, price) {
		if(!author) {
			throw new MissingDataError(`The property "author" is undefined!`)
		}
		if(!name) {
			throw new MissingDataError(`The property "name" is undefined!`)
		}
		if(!price) {
			throw new MissingDataError(`The property "price" is undefined!`)
		}
		this.author = author;
		this.name = name;
		this.price = price;
	}

	render(container) {
		const item = `<li>"${this.name}" - ${this.author}, ціна: ${this.price} грн.</li>`;
		container.insertAdjacentHTML('beforeend', item);
	}
}

const container = document.querySelector('#root'); 
const list = document.createElement('ul');
list.innerText = "Перелік доступних книжок:";
container.append(list);

books.forEach(el => {
	try {
		new Book(el.author, el.name, el.price).render(list);
	} catch(err) {
		if(err instanceof MissingDataError) {
			console.error(err);
		} else {
			throw err;
		}
	}
})

